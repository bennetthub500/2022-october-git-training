## Description

The Good Dogs Project recently published a blog post about how to give your dog a bath. The content was reviewed and edited before publication, but one of their readers pointed out a few issues in the post. Your job is to fix the errors the reader found.

## Your tasks

You need to complete three tasks. You'll complete your work in the `/content/blog/give-that-dog-a-bath.md` file.

### Fix link syntax

- [ ] Fix the broken link in the **Finding a location** section.

### Change headings that use -ing 

- [ ] Change the headings that use -ing form to simple verb form.

### Fix typos

- [ ] Fix the typos in the last paragraph.

## Before you open a merge request

- [ ] Review the issue to make sure you completed all tasks.
- [ ] Make sure that you committed your changes your branch, not `main`.